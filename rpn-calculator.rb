#Written by Adnan Noorani for RealPage

require_relative 'rpn-evaluator'

#Initialize RPN class
calculator = RPNEvaluator.new

begin
	
	#Prompt input
	print ">"
	
	#Obtain input and remove any carriage returns
	input = gets.chomp

	#Quit Condition
	quit = true if input == "q"

	if !quit
		puts calculator.evaluate(input)
	end

end while !quit