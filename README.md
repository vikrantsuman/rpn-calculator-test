#Ruby RPN Calculator
This RPN calculator was made by Adnan Noorani using Ruby for RealPage

##How to run
Clone this repository. From a CLI, navigate to the repository and run `ruby rpn-calculator.rb`

##How it works
The program takes a number, an operator, or `q` as the input. Any other input will prompt an error message.

If the input is `q`, the program will exit.

If the input is a number, the number will be added to the calculation stack.

If the input is an operator, the program will return the result of the corresponding operation on the top 2 numbers in the stack, display the result, and push the result back into the stack. An error message will be prompted if there are less than 2 operands in the stack when an operator is entered.

##Program Structure
The program is broken up into two sections, the calculator and the evaluator. The calculator is the executable that will take inputs and display outputs. It will communicate with the evaluator (RPNEvaluator class) to calculate the input expressions. The reason to break the program up like this is to mitigate the need to modify the evaluator class should the user interface change. Along with this, it will also be easier to incorporate more arithmetic functions in the evaluator class in the future. It also holds up OOP principles and is a generally good design practice to keep the logic separate from the user interface.

##Tradeoffs
Given more time, I would have liked to add in a secondary input style that could take in the whole RPN expression and parse it altogether, giving the end-user another input option. Additionally, I would have liked to build a web interface that takes an entire RPN expression string for processing (as that would be easier for the end-user instead of putting each number and operator in separately), as well as incorporate some more arithmetic operations, include exponents, roots, and factorial.

