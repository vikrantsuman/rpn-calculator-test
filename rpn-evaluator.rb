#Written by Adnan Noorani for RealPage

require 'bigdecimal'
require 'bigdecimal/util'

class RPNEvaluator

	def initialize
		@inputStack = Array.new
	end

	def evaluate(input)
		
		#Check if input is a number
		if input =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/
			@inputStack.push input.to_d
			return input

		#Check if input is an operator
		elsif input =~ /\A[+-\/\*]\Z/
			
			#Ensure at least two operands before operator
			if @inputStack.length < 2
				return "Error: Not enough operands"
			end

			result = calculate(input)

	    	#Push result into stack for subsequent use
			@inputStack.push(result)
		
		    #Display result
			return "Result: %g" % result

		else
			return "Error: Please input a number or operator"
		end	

	end

	private
	
	def calculate(operator)
		#Get operands from stack
        secondInput = @inputStack.pop
        firstInput = @inputStack.pop
        
        #Perform specified arithmetic operation
        case
			when operator == "+"
			  result = firstInput + secondInput
			when operator == "-"
			  result = firstInput - secondInput
			when operator == "*"
			  result = firstInput * secondInput
			when operator == "/"
			  result = firstInput / secondInput
		end
	end

end